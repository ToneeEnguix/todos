// DESCRIPTION //

This is a ToDo App. The best project to start testing your actual knowledge.

I built this as practice for the Barcelona Code School Full-Stack Bootcamp.

It is made using React and little CSS.

// LIVE DEMO // 

www.todoerapp.surge.sh