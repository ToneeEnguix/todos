import React from 'react'
import Item from './item'
import { v4 as uuid } from 'uuid'

var TodoList = (props) => {
  return(
    <div className="todosContainer">
      {props.todos.map( (item,i) => <Item item={item} i={i} completeTodo={props.completeTodo} deleteTodo={props.deleteTodo} key={uuid()}/>)}
    </div>

  )
}

export default TodoList

// class TodoList extends React.Component{

//   render() {
//     return <div>

//       {this.props.todos.map( item => item.content )}

//     </div>
//   }
// }