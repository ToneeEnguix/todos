import React from "react";

var Item = (props) => {
  return (
    <div className="container">
      <span className={`content ${props.item.completed}`}>
        {props.item.content}
      </span>
      <div className="container2">
        <span
          onClick={(e, idx) => props.completeTodo(e, props.i)}
          role="img"
          aria-label="xxxxx"
          className="pointer"
        >
          ✅
        </span>
        <span
          onClick={(e, idx) => props.deleteTodo(e, props.i)}
          role="img"
          aria-label="xxxxx"
          className="pointer"
        >
          ❌
        </span>
      </div>
    </div>
  );
};

export default Item;
