import React from "react";

var Info = () => {
  return (
    <div className="textContainer">
      <h1 className="title">ToDo List App</h1>
      <p className="text">
        Tired of hard to use, outdated apps?{" "}
        <span role="img" aria-label="xxxxx">
          🙆‍♀️
        </span>
      </p>
      <p className="text">
        <span role="img" aria-label="xxxxx">
          📓
        </span>{" "}
        Finally you can enjoy some easy organizer without having to worry about
        "How do I use it?!"{" "}
        <span role="img" aria-label="xxxxx">
          🤓
        </span>
      </p>
    </div>
  );
};

export default Info;
