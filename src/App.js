import React, { useState } from "react";
import "./index.css";
import { v4 as uuid } from "uuid";
import Info from "./components/info";
import TodoList from "./components/todoList";

var App = () => {
  var tempTodos = JSON.parse(localStorage.getItem("todos")) || [];

  const [todos, setTodos] = useState(tempTodos);
  const [text, setText] = useState("");

  const handleChange = (e) => {
    setText(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let todosHere = [...todos];
    todosHere.push({ id: uuid(), content: text, completed: false });
    setTodos(todosHere);
    localStorage.setItem("todos", JSON.stringify(todosHere));
    setText("");
  };
  const completeTodo = (e, i) => {
    let todosHere = [...todos];
    todosHere[i].completed = !todosHere[i].completed;
    setTodos(todosHere);
    localStorage.setItem("todos", JSON.stringify(todosHere));
  };
  const deleteTodo = (e, i) => {
    let todosHere = [...todos];
    todosHere.splice(i, 1);
    setTodos(todosHere);
    localStorage.setItem("todos", JSON.stringify(todosHere));
  };

  return (
    <div className="main">
      <Info />
      <form onSubmit={handleSubmit} className="form">
        <input
          placeholder="What to do...?"
          onChange={(e) => handleChange(e)}
          value={text}
          className="input"
        />
        <button className="addButton pointer">Add todo</button>
      </form>
      <TodoList
        todos={todos}
        completeTodo={completeTodo}
        deleteTodo={deleteTodo}
      />
    </div>
  );
};

export default App;

// class App extends React.Component {

//   state = {
//     todos: [],
//     text: '',
//   }

//   handleChange = e => {
//     var data = e.target.value
//     let {text} = {...this.state}
//     text = data
//     this.setState({text})
//   }
//   handleSubmit = (e) => {
//     e.preventDefault()
//     let {todos} = {...this.state}
//     todos.push({'id': uuid(), 'content': this.state.text, completed: false})
//     this.setState( {todos, text: ''}, ()=>console.log(this.state.todos) )
//   }
//   renderTodos = () => {
//     return this.state.todos.map( (item, i) => {
//       return item.completed === false ? (
//         <div key={uuid()}>
//           <span onClick={(e,idx)=>this.completeTodo(e,i)} role="img" aria-label="xxxxx">✅</span>
//           <span className="content">{item.content}</span>
//           <span onClick={(e,idx)=>this.deleteTodo(e,i)} role="img" aria-label="xxxxx">❌</span>
//         </div>
//        ) : item.completed === true &&  (
//         <div key={uuid()}>
//           <span onClick={(e,idx)=>this.completeTodo(e,i)} role="img" aria-label="xxxxx">✅</span>
//           <span className="content true">{item.content}</span>
//           <span onClick={(e,idx)=>this.deleteTodo(e,i)} role="img" aria-label="xxxxx">❌</span>
//         </div>
//        )
//     })
//   }
//   completeTodo = (e,i) => {
//     let {todos} = {...this.state}
//     todos[i].completed = !todos[i].completed
//     // style = {textDecorationLine: 'line-through'}
//     this.setState( {todos}, ()=>console.log(this.state.todos) )
//   }
//   deleteTodo = (e,i) => {
//     let {todos} = {...this.state}
//     todos.splice(i, 1)
//     this.setState( {todos}, ()=>console.log(this.state.todos) )
//   }

//   render() {
//     return <div className="main">
//       <main>
//         <h1 className="title">ToDo List App</h1>
//         <p className="text">Tired of hard to use, outdated apps? <span role="img" aria-label="xxxxx">🙆‍♀️</span></p>
//         <p className="text"><span role="img" aria-label="xxxxx">📓</span> Finally you can enjoy some easy organizer without having to worry about "How do I use it?!" <span role="img" aria-label="xxxxx">🤓</span></p>
//         <form onSubmit={this.handleSubmit}>
//           <input placeholder="What to do...?" onChange={this.handleChange} value={this.state.text}/>
//           <button className="addButton">Add todo</button>
//         </form>
//         {/* <div><Todos todos={this.state.todos}/></div> */}
//         {this.renderTodos()}
//       </main>
//     </div>
//   }

// }

// add README.md
// add alert of completed todos
// add DB
// OAuth
